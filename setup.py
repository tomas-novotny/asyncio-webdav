from setuptools import setup, find_packages

properties = dict(
    name="asyncio-webdav",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3.6",
    ],
    description="A straight-forward WebDAV client, implemented using Requests",
    license="MIT",
    author="Tomas Novotny",
    author_email="novott20@fit.cvut.cz",
    url="https://github.com/novott20/asyncio-webdav",
    version="0.0.1",
    packages=find_packages(exclude=["tests"]),
    data_files=[],
    install_requires=[
        "requests",
    ],
    entry_points=dict(
        console_scripts=[],
    ),
)

setup(**properties)
